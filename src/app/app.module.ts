import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component'
import { InicioComponent } from './inicio/inicio.component'
import { FornecedoresComponent } from './fornecedor/fornecedores.component'
import { FornecedorComponent } from './fornecedor/fornecedor.component'
import { ProdutosComponent } from './produto/produtos.component'
import { ProdutoComponent } from './produto/produto.component'
import { PromocoesComponent } from './promocao/promocoes.component'
import { PromocaoComponent } from './promocao/promocao.component'
import { VendasComponent } from './venda/vendas.component'
import { VendaComponent } from './venda/venda.component'
import { ClientesComponent } from './cliente/clientes.component'
import { ClienteComponent } from './cliente/cliente.component'
import { FuncionariosComponent } from './funcionario/funcionarios.component'
import { FuncionarioComponent } from './funcionario/funcionario.component'
import { LoginComponent } from './login/login.component'
import { RecuperarSenhaComponent } from './recuperar-senha/recuperar-senha.component'
import { ProdutoService } from './produto/produto.service'

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'inicio', component: InicioComponent },
      { path: 'fornecedores', component: FornecedoresComponent },
      { path: 'fornecedores/editar', component: FornecedorComponent },
      { path: 'produtos', component: ProdutosComponent },
      { path: 'produtos/:id', component: ProdutoComponent },
      { path: 'produtos/editar', component: ProdutoComponent },
      { path: 'promocoes', component: PromocoesComponent },
      { path: 'promocoes/editar', component: PromocaoComponent },
      { path: 'vendas', component: VendasComponent },
      { path: 'vendas/editar', component: VendaComponent },
      { path: 'clientes', component: ClientesComponent },
      { path: 'clientes/editar', component: ClienteComponent },
      { path: 'funcionarios', component: FuncionariosComponent },
      { path: 'funcionarios/editar', component: FuncionarioComponent },
      { path: 'login', component: LoginComponent },
      { path: 'recuperar-senha', component: RecuperarSenhaComponent },
      { path: '', pathMatch: 'full', redirectTo: 'inicio' }
    ]),
    NgbModule.forRoot()
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    FornecedoresComponent,
    FornecedorComponent,
    ProdutosComponent,
    ProdutoComponent,
    PromocoesComponent,
    PromocaoComponent,
    VendasComponent,
    VendaComponent,
    ClientesComponent,
    ClienteComponent,
    FuncionariosComponent,
    FuncionarioComponent,
    LoginComponent,
    RecuperarSenhaComponent
  ],
  providers: [ProdutoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
