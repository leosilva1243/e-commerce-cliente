import { Component, Input, AfterViewInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { Produto } from './produto.model'
import { ProdutoService } from './produto.service'

@Component({
    moduleId: module.id,
    selector: 'produto',
    templateUrl: 'produto.component.html'
})
export class ProdutoComponent {
    @Input()
    produto: Produto = {
        id: 0,
        categoria: '',
        descricao: '',
        dimensoes: {
            altura: 0, largura: 0, profundidade: 0
        },
        fabricante: '',
        identificacao: '',
        peso: 0,
        preco: 0
    }

    constructor(
        private readonly service: ProdutoService,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) { }

    ngAfterViewInit() {
        this.route.params.subscribe(params => {
            const id = +params['id']
            this.service.find(id)
                .then(produto => this.produto = produto)
        })
    }

    private async salvar() {
        await this.service.update(this.produto)
        this.router.navigate(['..'], { relativeTo: this.route })
    }

    private async criar() {
        await this.service.add(this.produto)
        this.router.navigate(['..'], { relativeTo: this.route })
    }

    private async excluir() {
        await this.service.delete(this.produto.id)
        this.router.navigate(['..'], { relativeTo: this.route })
    }
}