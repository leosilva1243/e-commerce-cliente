import { Component, AfterViewInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

import { ProdutoService } from './produto.service'
import { Produto } from './produto.model'

@Component({
    moduleId: module.id,
    selector: 'produtos',
    templateUrl: 'produtos.component.html'
})
export class ProdutosComponent implements AfterViewInit {
    produtos: Produto[] = []

    constructor(
        private readonly service: ProdutoService,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) { }

    ngAfterViewInit() {
        this.carregarProdutos()
    }

    private async selecionarProduto(produto: Produto) {
        this.router.navigate([produto.id], { relativeTo: this.route })
    }

    private async carregarProdutos() {
        try {
            this.produtos = await this.service.list()
        } catch (error) {
            console.error(error)
            alert('Erro ao listar produtos')
        }
    }
}