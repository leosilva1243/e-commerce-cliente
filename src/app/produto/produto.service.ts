import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import { Observable } from 'rxjs/Rx';

import { Produto } from './produto.model'

@Injectable()
export class ProdutoService {
    private readonly url = 'http://localhost:8080/produtos'

    constructor(
        private readonly http: Http
    ) { }

    async find(id: number): Promise<Produto | undefined> {
        return this.http.get(`${this.url}/${id}`)
            .map(response => response.json() as Produto)
            .toPromise()
    }

    async list(): Promise<Produto[]> {
        return this.http.get(this.url)
            .map(response => response.json() as Produto[])
            .toPromise()
    }

    async add(entity: Produto): Promise<Produto> {
        return this.http.post(this.url, entity)
            .map(response => response.json() as Produto)
            .toPromise()
    }

    async update(entity: Produto): Promise<void> {
        return this.http.put(`${this.url}/${entity.id}`, entity)
            .map(() => {})
            .toPromise()
    }

    async delete(id: number): Promise<Produto> {
        return this.http.delete(`${this.url}/${id}`)
            .map(response => response.json() as Produto)
            .toPromise()
    }
}